#!/usr/bin/env python3
#
# Copyright (C) 2021 Soichiro Kuwahara
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""Cherenkov burst plotting summary tool"""

#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#

import itertools
import math
from gstlal.plots import set_matplotlib_cache_directory
set_matplotlib_cache_directory()
import matplotlib
from matplotlib import figure
from matplotlib import patches
matplotlib.rcParams.update({
	"font.size": 12.0,
	"axes.titlesize": 12.0,
	"axes.labelsize": 12.0,
	"xtick.labelsize": 12.0,
	"ytick.labelsize": 12.0,
	"legend.fontsize": 10.0,
	"figure.dpi": 300,
	"savefig.dpi": 300,
	"text.usetex": True,
	"path.simplify": True,
	"font.family": "serif"
})
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import numpy
from scipy import interpolate
from scipy import optimize
import sys

from optparse import OptionParser

from lal import rate
from lalburst import binjfind
import lalsimulation
from lal.utils import CacheEntry
from gstlal import cherenkov
from gstlal import stats as gstlalstats
from gstlal.plots import util as gstlalplotutil
from gstlal.cherenkov import rankingstat as cherenkov_rankingstat

from ligo.lw.utils import process as ligolw_process
from ligo.lw import utils as ligolw_utils
from ligo.lw import lsctables


#
# =============================================================================
#
#                                 Command Line
#
# =============================================================================
#


def parse_command_line():
	parser = OptionParser()
	parser.add_option("--candidates-cache", metavar = "filename", help = "Also load the candidates from files listed in this LAL cache.  See lalapps_path2cache for information on how to produce a LAL cache file.")
	parser.add_option("--power", type = "float", help = "Power of the source. It is used for making the contour graph of true rate.")
	parser.add_option("--ranking-stat-pdf", metavar = "filename", help = "Set the name of the xml file rankingstatpdf.")
	parser.add_option("--verbose", "-v", action = "store_true", help = "Be verbose.")

	options, urls = parser.parse_args()

	if options.ranking_stat_pdf is None:
		options.ranking_stat_pdf = []
	if options.candidates_cache is not None:
		urls += [CacheEntry(line).url for line in open(options.candidates_cache)]
	if not options.ranking_stat_pdf:
		raise ValueError("must provide at least one ranking statistic file")
	if not options.candidates_cache:
		raise ValueError("must provide at least one output file")

	return options, urls


#
# =============================================================================
#
#                                Misc Utilities
#
# =============================================================================
#


def create_plot(x_label = None, y_label = None, width = 165.0, aspect = gstlalplotutil.golden_ratio):
	fig = figure.Figure()
	FigureCanvas(fig)
	fig.set_size_inches(width / 25.4, width / 25.4 / aspect)
	axes = fig.add_axes((0.1, 0.12, .875, .80))
	axes.grid(True)
	if x_label is not None:
		axes.set_xlabel(x_label)
	if y_label is not None:
		axes.set_ylabel(y_label)
	return fig, axes


def is_injection_document(xmldoc):
	try:
		lsctables.SimBurstTable.get_table(xmldoc)
	except ValueError:
		return False
	return True


#
# =============================================================================
#
#                       False alarm rates/probabilities 
#
# =============================================================================
#


#
# Class to compute false-alarm probabilities and false-alarm rates from
# ranking statistic PDFs
#

class FAPFAR(object):
	def __init__(self, rankingstatpdf):
		# obtain livetime from rankingstatpdf
		# FIXME: temporaly unavailable
		#self.livetime = float(abs(rankingstatpdf.segments))

		# set the rate normalization LR threshold to the mode of
		# the zero-lag LR distribution
		rate_normalization_lr_threshold, = rankingstatpdf.zl_lr_lnpdf.argmax()
		# FIXME:  override with a manual cut until an automatic algorithm can be found
		rate_normalization_lr_threshold = -19.

		# record trials factor, with safety checks
		counts = rankingstatpdf.zl_lr_lnpdf.count
		self.count_above_threshold = counts[rate_normalization_lr_threshold:,].sum()

		# get noise model ranking stat value and event count from
		# bins
		threshold_index = rankingstatpdf.noise_lr_lnpdf.bins[0][rate_normalization_lr_threshold]
		ranks = rankingstatpdf.noise_lr_lnpdf.bins[0].lower()[threshold_index:]
		counts = rankingstatpdf.noise_lr_lnpdf.array[threshold_index:]

		# complementary cumulative distribution function
		ccdf = counts[::-1].cumsum()[::-1]
		ccdf /= ccdf[0]

		# ccdf is P(ranking statistic > threshold | a candidate).
		# we need P(ranking statistic > threshold), i.e. need to
		# correct for the possibility that no candidate is present.
		# specifically, the ccdf needs to =1-1/e at the candidate
		# identification threshold, and cdf=1/e at the candidate
		# threshold, in order for FAP(threshold) * live time to
		# equal the actual observed number of candidates.
		ccdf = gstlalstats.poisson_p_not_0(ccdf)

		# build interpolator
		self.ccdf_interpolator = interpolate.interp1d(ranks, ccdf)

		# record min and max ranks so we know which end of the ccdf
		# to use when we're out of bounds
		self.minrank = ranks[0]
		self.maxrank = ranks[-1]

	@property
	def threshold(self):
		"""
		Noise model event rate prediction only valid at or above
		this value of ranking statistic.
		"""
		return self.minrank

	@gstlalstats.assert_probability
	def ccdf_from_rank(self, rank):
		return self.ccdf_interpolator(numpy.clip(rank, self.minrank, self.maxrank))

	def fap_from_rank(self, rank):
		# implements equation (8) from Phys. Rev. D 88, 024025.
		# arXiv: 1209.0718
		return gstlalstats.fap_after_trials(self.ccdf_from_rank(rank), self.count_above_threshold)

	def far_from_rank(self, rank):
		# implements equation (B4) of Phys. Rev. D 88, 024025.
		# arXiv: 1209.0718. the return value is divided by T to
		# convert events/experiment to events/second. "tdp" = 
		# true-dismissal probability = 1 - single-event FAP, the
		# integral in equation (B4)
		log_tdp = numpy.log1p(-self.ccdf_from_rank(rank))
		return self.count_above_threshold * -log_tdp# / self.livetime


#
# =============================================================================
#
#                            Ranking Stat PDF Plots
#
# =============================================================================
#


def rankingstat_pdfs_plot(rankingstatpdf):
	fig = figure.Figure()
	FigureCanvas(fig)
	axes = fig.gca()

	line1, = axes.semilogy(rankingstatpdf.noise_lr_lnpdf.bins[0].centres(), numpy.exp(rankingstatpdf.noise_lr_lnpdf.at_centres()), linewidth = 1)
	rankingstatpdf.zl_lr_lnpdf.normalize()
	axes.semilogy(rankingstatpdf.zl_lr_lnpdf.bins[0].centres(), numpy.exp(rankingstatpdf.zl_lr_lnpdf.at_centres()), color = 'red')
	axes.set_xlim(-27., 5)
	axes.set_ylim(1e-4, 1e1) 
	axes.set_xlabel("$\ln\mathcal{L}$")
	axes.set_ylabel("$P$")

	return fig


#
# =============================================================================
#
#                           Rate vs. Threshold Plots
#
# =============================================================================
#


def sigma_region(mean, nsigma):
	return numpy.concatenate((mean - nsigma * numpy.sqrt(mean), (mean + nsigma * numpy.sqrt(mean))[::-1]))

def create_rate_vs_lnL_plot(axes, stats, stats_label, fapfar):
	axes.semilogy()

	#
	# plot limits and expected counts
	#

	#xlim = fapfar.threshold, max(2. * math.ceil(stats.max() / 2.), 10.)
	xlim = fapfar.threshold, 20.
	#ylim = 5e-7, 10.**math.ceil(math.log10(expected_count(xlim[0])))
	#xlim = -0.1, max(2. * math.ceil(stats.max() / 2.), 10.)
	ylim = 1e-6, 1e5

	#
	# expected count curve
	#

	expected_count_x = numpy.linspace(xlim[0], xlim[1], 10000)
	expected_count_y = fapfar.far_from_rank(expected_count_x)
	line1, = axes.plot(expected_count_x, expected_count_y, 'k--', linewidth = 1)

	#
	# error bands
	#

	expected_count_x = numpy.concatenate((expected_count_x, expected_count_x[::-1]))
	line2, = axes.fill(expected_count_x, sigma_region(expected_count_y, 3.0).clip(*ylim), alpha = 0.25, facecolor = [0.75, 0.75, 0.75])
	line3, = axes.fill(expected_count_x, sigma_region(expected_count_y, 2.0).clip(*ylim), alpha = 0.25, facecolor = [0.5, 0.5, 0.5])
	line4, = axes.fill(expected_count_x, sigma_region(expected_count_y, 1.0).clip(*ylim), alpha = 0.25, facecolor = [0.25, 0.25, 0.25])

	#
	# observed
	#

	if stats is not None:
		N = numpy.arange(1., len(stats) + 1., dtype = "double")
		line5, = axes.plot(stats.repeat(2)[1:], N.repeat(2)[:-1], 'k', linewidth = 2)

	#
	# legend
	#

	axes.legend((line5, line1, line4, line3, line2), (stats_label, r"Noise Model, $\langle N \rangle$", r"$\pm\sqrt{\langle N \rangle}$", r"$\pm 2\sqrt{\langle N \rangle}$", r"$\pm 3\sqrt{\langle N \rangle}$"), loc = "upper right")

	#
	# adjust bounds of plot
	#

	axes.set_xlim(xlim)
	axes.set_ylim(ylim)


def RateVsThreshold(fapfar, zerolag_stats, background_stats):
	zerolag_fig, axes = create_plot(r"$\ln \mathcal{L}$ Threshold", "Number of Events $\geq \ln \mathcal{L}$")
	zerolag_stats = numpy.array(sorted(zerolag_stats, reverse = True))
	create_rate_vs_lnL_plot(axes, zerolag_stats, r"Observed", fapfar)
	axes.set_title(r"Event Count vs.\ Ranking Statistic Threshold")

	background_fig, axes = create_plot(r"$\ln \mathcal{L}$ Threshold", "Number of Events $\geq \ln \mathcal{L}$")
	background_stats = numpy.array(sorted(background_stats, reverse = True))
	create_rate_vs_lnL_plot(axes, background_stats, r"Observed (time shifted)", fapfar)
	axes.set_title(r"Event Count vs.\ Ranking Statistic Threshold (Closed Box)")
	detection_threshold = zerolag_stats[0]
	return zerolag_fig, background_fig, detection_threshold


#
# =============================================================================
#
#                               Efficeicny curve
#
# =============================================================================
#


def slope(x, y):
	"""
	From the x and y arrays, compute the slope at the x co-ordinates
	using a first-order finite difference approximation.
	"""
	slope = numpy.zeros((len(x),), dtype = "double")
	slope[0] = (y[1] - y[0]) / (x[1] - x[0])
	for i in range(1, len(x) - 1):
		slope[i] = (y[i + 1] - y[i - 1]) / (x[i + 1] - x[i - 1])
	slope[-1] = (y[-1] - y[-2]) / (x[-1] - x[-2])
	return slope


def upper_err(y, yerr, deltax):
	z = y + yerr
	deltax = int(deltax)
	upper = numpy.zeros((len(yerr),), dtype = "double")
	for i in range(len(yerr)):
		upper[i] = max(z[max(i - deltax, 0) : min(i + deltax, len(z))])
	return upper - y


def lower_err(y, yerr, deltax):
	z = y - yerr
	deltax = int(deltax)
	lower = numpy.zeros((len(yerr),), dtype = "double")
	for i in range(len(yerr)):
		lower[i] = min(z[max(i - deltax, 0) : min(i + deltax, len(z))])
	return y - lower


def render_from_bins(axes, efficiency_numerator, efficiency_denominator, cal_uncertainty, filter_width, colour = "k", erroralpha = 0.3, linestyle = "-"):
	# extract array of x co-ordinates, and the factor by which x
	# increases from one sample to the next.
	(x,) = efficiency_denominator.centres()
	x_factor_per_sample = efficiency_denominator.bins[0].delta

	# compute the efficiency, the slope (units = efficiency per
	# sample), the y uncertainty (units = efficiency) due to binomial
	# counting fluctuations, and the x uncertainty (units = samples)
	# due to the width of the smoothing filter.
	eff = efficiency_numerator.array / efficiency_denominator.array
	dydx = slope(numpy.arange(len(x), dtype = "double"), eff)
	yerr = numpy.sqrt(eff * (1. - eff) / efficiency_denominator.array)
	xerr = numpy.array([filter_width / 2.] * len(yerr))

	# compute the net y err (units = efficiency) by (i) multiplying the
	# x err by the slope, (ii) dividing the calibration uncertainty
	# (units = percent) by the fractional change in x per sample and
	# multiplying by the slope, (iii) adding the two in quadradure with
	# the y err.
	net_yerr = numpy.sqrt((xerr * dydx)**2. + yerr**2. + (cal_uncertainty / x_factor_per_sample * dydx)**2.)

	# compute net xerr (units = percent) by dividing yerr by slope and
	# then multiplying by the fractional change in x per sample.
	net_xerr = net_yerr / dydx * x_factor_per_sample

	# plot the efficiency curve and uncertainty region
	#patch = patches.Polygon(zip(numpy.concatenate((x, x[::-1])), numpy.concatenate((eff + upper_err(eff, yerr, filter_width / 2.), (eff - lower_err(eff, yerr, filter_width / 2.))[::-1]))), edgecolor = colour, facecolor = colour, alpha = erroralpha)
	#axes.add_patch(patch)
	line, = axes.plot(x, eff, colour + linestyle)

	# compute 50% point and its uncertainty
	A50 = None #optimize.bisect(interpolate.interp1d(x, eff - 0.5), x[0], x[-1], xtol = 1e-40)
	A50_err = None #interpolate.interp1d(x, net_xerr)(A50)

	# print some analysis FIXME:  this calculation needs attention
	num_injections = efficiency_denominator.array.sum()
	num_samples = len(efficiency_denominator.array)
	#print("Bins were %g samples wide, ideal would have been %g" % (filter_width, (num_samples / num_injections / interpolate.interp1d(x, dydx)(A50)**2.0)**(1.0/3.0)), file=sys.stderr)
	#print("Average number of injections in each bin = %g" % efficiency_denominator.array.mean(), file=sys.stderr)

	return line, A50, A50_err


def create_efficiency_plot(axes, all_injections, found_injections, detection_threshold):
	# formats
	axes.semilogx()
	axes.set_position([0.10, 0.150, 0.86, 0.77])
	
	# set desired yticks
	axes.set_yticks((0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0))
	axes.set_yticklabels((r"\(0\)", r"\(0.1\)", r"\(0.2\)", r"\(0.3\)", r"\(0.4\)", r"\(0.5\)", r"\(0.6\)", r"\(0.7\)", r"\(0.8\)", r"\(0.9\)", r"\(1.0\)"))

	axes.xaxis.grid(True, which = "major, minor")
	axes.yaxis.grid(True, which = "major, minor")

	# the denominator and numerator binning for the plot
	efficiency_numerator = rate.BinnedArray(rate.NDBins((rate.LogarithmicBins(min(sim.amplitude for sim in all_injections), max(sim.amplitude for sim in all_injections), 400),)))
	efficiency_denominator = rate.BinnedArray(efficiency_numerator.bins)

	# add into the vins
	for sim in found_injections:
		efficiency_numerator[sim.amplitude,] += 1
	for sim in all_injections:
		efficiency_denominator[sim.amplitude,] += 1

	# adjust window function normalization
	filter_width = 10
	windowfunc = rate.gaussian_window(filter_width)
	windowfunc /= windowfunc[int((len(windowfunc) + 1) / 2)]
	rate.filter_array(efficiency_numerator.array, windowfunc)
	rate.filter_array(efficiency_denominator.array, windowfunc)

	# regularize: adjust unused bins so that the efficiency is 0 avoid Nan
	assert(efficiency_numerator.array <= efficiency_denominator.array).all()
	efficiency_denominator.array[(efficiency_numerator.array == 0) & (efficiency_denominator.array == 0)] = 1
	
	# FIXME: for output checking only
	eff = rate.BinnedArray(efficiency_numerator.bins, array = efficiency_numerator.array / efficiency_denominator.array)
	#print(eff, file = sys.stderr)

	line1, A50, A50_error = render_from_bins(axes, efficiency_numerator, efficiency_denominator, 0.2, filter_width)

	# add a legend to the axis
	axes.legend((line1,), (r"\noindent Injections with $\log \Lambda > %.2f$" % detection_threshold,), loc = "lower right")

	# adjust limits
	axes.set_ylim([0.0, 1.0])

	# return eff for true rate contour
	return eff


def Efficiency(all_injections, found_injections, detection_threshold):
	fig, axes = create_plot(r"Injection Amplitude ($\mathrm{J/m^2}$)", "Detection Efficiency")
	eff = create_efficiency_plot(axes, all_injections, found_injections, detection_threshold)
	axes.set_title(r"Detection Efficiency vs.\ Amplitude")
	return fig, eff


#
# =============================================================================
#
#                              Constrains Contour
#
# =============================================================================
#


def create_truerate_contour(axes, power, eff):
	# set beta and impact parameter axis
	rate = numpy.zeros((300,300))
	beta = 10.**numpy.linspace(0.01, 2., 300)
	r = 10.**numpy.linspace(5., 9., 300)

	print(eff, file = sys.stderr)
	# calculate
	for i in range(len(beta)):
		for j in range(len(r)):
			dE_dA = lalsimulation.SimBurstCherenkov_dE_dA(power, beta[i], r[j])
			dE_dA = max(dE_dA, eff.bins[0].min)
			dE_dA = min(dE_dA, eff.bins[0].max)
			rate[i][j] = 3.890 / eff[dE_dA,]

	# set color maps
	norm = matplotlib.colors.LogNorm(vmin = 1., vmax = 10000.)
	cmap = matplotlib.cm.viridis
	cmap.set_bad("k")
	mesh = axes.pcolormesh(beta, r, rate.T, norm = norm, cmap = cmap, shading = "gouraud")
	contour = axes.contour(beta, r, rate.T, norm = norm, colors = "black", linestyle = "-", linewidth = .5, alpha = 1., levels = numpy.logspace(0., 3., 7, base = 10.))
	fmt = matplotlib.ticker.LogFormatterMathtext()
	axes.clabel(contour, contour.levels, colors = "black", fmt = fmt)
	axes.loglog()
	axes.grid(which = "both")

def Truerate(power, eff):
	fig, axes = create_plot(r"$\beta$", "Impact parameter(metres)")
	create_truerate_contour(axes, power, eff)
	axes.set_title(r"90\%% Confidence Rate Upper Limit for NCC-1701-D Enterprise ($P=%s$ W)" % gstlalplotutil.latexnumber("%.4g" % power))
	return fig

#
# =============================================================================
#
#                                  Main
#
# =============================================================================
#


#
# Parse command line
#


options, urls = parse_command_line()


#
# load ranking statistic PDFs
#


rankingstatpdf = cherenkov_rankingstat.RankingStatPDF.from_xml(ligolw_utils.load_filename(options.ranking_stat_pdf, verbose = options.verbose, contenthandler = cherenkov_rankingstat.LIGOLWContentHandler), "gstlal_cherenkov_rankingstat_pdf")
fapfar = FAPFAR(rankingstatpdf)


#
# plot PDFs
#


fig = rankingstat_pdfs_plot(rankingstatpdf)
for ext in (".png", ".pdf"):
	filename = "rankingstat_pdfs"
	if options.verbose:
		print("writing %s%s ..." % (filename, ext), file = sys.stderr)
	fig.savefig("%s%s" % (filename, ext))


#
# load zero-lag candidates.
#


zerolag_stats = []
background_stats = []

for n, url in enumerate(urls, 1):
	if options.verbose:
		print("%d/%d: " % (n, len(urls)), end = "", file = sys.stderr)
	xmldoc = ligolw_utils.load_url(url, contenthandler = cherenkov_rankingstat.LIGOLWContentHandler, verbose = options.verbose)
	if is_injection_document(xmldoc):
		continue
	coinc_def_id = lsctables.CoincDefTable.get_table(xmldoc).get_coinc_def_id(search = cherenkov.CherenkovBBCoincDef.search, search_coinc_type = cherenkov.CherenkovBBCoincDef.search_coinc_type, create_new = False)
	zl_time_slide_ids = frozenset(time_slide_id for time_slide_id, offsetvector in lsctables.TimeSlideTable.get_table(xmldoc).as_dict().items() if not any(offsetvector.values()))

	for coinc in lsctables.CoincTable.get_table(xmldoc):
		if coinc.coinc_def_id != coinc_def_id:
			continue
		if coinc.time_slide_id in zl_time_slide_ids:
			zerolag_stats.append(coinc.likelihood)
		else:
			background_stats.append(coinc.likelihood)
	if options.verbose:
		print("now %d background events" % len(background_stats), file = sys.stderr)


#
# rate_vs_thresh "expected from noise model"
#


rate_vs_thresh_zl, rate_vs_thresh_bg, threshold = RateVsThreshold(fapfar, zerolag_stats, background_stats)


for filename, fig in [("rate_vs_threshold_open", rate_vs_thresh_zl), ("rate_vs_threshold", rate_vs_thresh_bg)]:
	for ext in (".png", ".pdf"):
		if options.verbose:
			print("writing %s%s ..." % (filename, ext), file = sys.stderr)
		fig.savefig("%s%s" % (filename, ext))


#
# load injection files
#


all_injections = []
found_injections = []
is_injection = False

for n, url in enumerate(urls, 1):
	if options.verbose:
		print("%d/%d: " % (n, len(urls)), end = "", file = sys.stderr)
	xmldoc = ligolw_utils.load_url(url, contenthandler = cherenkov_rankingstat.LIGOLWContentHandler, verbose = options.verbose)

	if not is_injection_document(xmldoc):
		continue
	is_injection = True
	coinc_def_id = lsctables.CoincDefTable.get_table(xmldoc).get_coinc_def_id(search = cherenkov.CherenkovBBCoincDef.search, search_coinc_type = cherenkov.CherenkovBBCoincDef.search_coinc_type, create_new = False)
	lnL_index = dict((coinc.coinc_event_id, coinc.likelihood) for coinc in lsctables.CoincTable.get_table(xmldoc) if coinc.coinc_def_id == coinc_def_id)

	coinc_def_id = lsctables.CoincDefTable.get_table(xmldoc).get_coinc_def_id(search = binjfind.CherenkovSBCNearCoincDef.search, search_coinc_type = binjfind.CherenkovSBCNearCoincDef.search_coinc_type, create_new = False)
	coinc_event_ids = frozenset(coinc.coinc_event_id for coinc in lsctables.CoincTable.get_table(xmldoc) if coinc.coinc_def_id == coinc_def_id)

	sim_lnL_index = {}
	for coinc_event_id, rows in itertools.groupby(sorted(lsctables.CoincMapTable.get_table(xmldoc), key = lambda row: row.coinc_event_id), lambda row: row.coinc_event_id):
		if coinc_event_id not in coinc_event_ids:
			continue
		rows = tuple(rows)
		simulation_id, = (row.event_id for row in rows if row.table_name == "sim_burst")
		sim_lnL_index[simulation_id] = max(lnL_index[row.event_id] for row in rows if row.table_name == "coinc_event")

	for sim in lsctables.SimBurstTable.get_table(xmldoc):
		#FIXME: In the injection generation process we picked the time when two detectors on it is better if we can check again in here.
		all_injections.append(sim)

		if sim_lnL_index.get(sim.simulation_id, float("-inf")) >= threshold:
			found_injections.append(sim)

if is_injection:
	efficiencyfig, eff = Efficiency(all_injections, found_injections, threshold)

	for ext in (".png", ".pdf"):
		if options.verbose:
			print("writing Efficiency%s with threshold of %s" % (ext,threshold), file = sys.stderr)
		efficiencyfig.savefig("Efficiency%s" % ext)

	if options.power is not None:
		rate = Truerate(options.power, eff)

		for ext in (".png", ".pdf"):
			if options.verbose:
				print("writing True rate contour graph", file = sys.stderr)
			rate.savefig("Truerate%s" % ext)
