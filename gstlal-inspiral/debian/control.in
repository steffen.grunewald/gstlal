Source: gstlal-inspiral
Maintainer: Carsten Aulbert <carsten.aulbert@aei.mpg.de>
Section: lscsoft
Priority: optional
Standards-Version: 3.9.2
X-Python3-Version: >= @MIN_PYTHON_VERSION@
Build-Depends:
 debhelper (>= 9),
 dh-python,
 doxygen (>= @MIN_DOXYGEN_VERSION@),
 fakeroot,
 gobject-introspection (>= @MIN_GOBJECT_INTROSPECTION_VERSION@),
 graphviz,
 gstlal-dev (>= @MIN_GSTLAL_VERSION@),
 gstlal-ugly-dev (>= @MIN_GSTLALUGLY_VERSION@),
 gtk-doc-tools (>= @MIN_GTK_DOC_VERSION@),
 liblal-dev (>= @MIN_LAL_VERSION@),
 liblalinspiral-dev (>= @MIN_LALINSPIRAL_VERSION@),
 liblalmetaio-dev (>= @MIN_LALMETAIO_VERSION@),
 libgirepository1.0-dev (>= @MIN_GOBJECT_INTROSPECTION_VERSION@),
 libgsl-dev (>= 1.9),
 libgstreamer1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 libgstreamer-plugins-base1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 pkg-config (>= @MIN_PKG_CONFIG_VERSION@),
 python3-all-dev (>= @MIN_PYTHON_VERSION@),
 python3-gi, python-gi-dev,
 python3-lal (>= @MIN_LAL_VERSION@),
 python3-lalinspiral (>= @MIN_LALINSPIRAL_VERSION@)

Package: gstlal-inspiral
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends},
 gstlal (>= @MIN_GSTLAL_VERSION@),
 gstlal-ugly (>= @MIN_GSTLALUGLY_VERSION@),
 lal (>= @MIN_LAL_VERSION@),
 lalinspiral (>= @MIN_LALINSPIRAL_VERSION@),
 lalapps,
 lalmetaio (>= @MIN_LALMETAIO_VERSION@),
 libgirepository-1.0-1 (>= @MIN_GOBJECT_INTROSPECTION_VERSION@),
 libgstreamer1.0-0 (>= @MIN_GSTREAMER_VERSION@),
 libgstreamer-plugins-base1.0-0 (>= @MIN_GSTREAMER_VERSION@),
 ligo-scald (>= @MIN_LIGO_SCALD_VERSION@),
 python3 (>= @MIN_PYTHON_VERSION@),
 python3-gi,
 python3-gst-1.0,
 python3-h5py,
 python3-gwdatafind,
 python3-lal (>= @MIN_LAL_VERSION@),
 python3-lalinspiral (>= @MIN_LALINSPIRAL_VERSION@),
 python3-ligo-gracedb (>= 2.7.5),
 python3-ligo-lw (>= @MIN_LIGO_LW_VERSION@),
 python3-ligo-lw-bin (>= @MIN_LIGO_LW_VERSION@),
 python3-ligo-segments (>= @MIN_LIGO_SEGMENTS_VERSION@),
 python3-ligo-scald (>= @MIN_LIGO_SCALD_VERSION@),
 python3-matplotlib,
 python3-numpy (>= @MIN_NUMPY_VERSION@),
 python3-scipy,
 python3-tqdm
Description: GStreamer for GW data analysis (inspiral parts)
 This package provides a variety of gstreamer elements for
 gravitational-wave data analysis and some libraries to help write such
 elements.  The code here sits on top of several other libraries, notably
 the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
 and, of course, GStreamer.
 This package contains plugins, libraries, and programs for inspiral data
 analysis.

Package: gstlal-inspiral-dev
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, 
 gstlal-dev (>= @MIN_GSTLAL_VERSION@),
 gstlal-inspiral (= ${binary:Version}),
 lal-dev (>= @MIN_LAL_VERSION@),
 lalinspiral-dev (>= @MIN_LALINSPIRAL_VERSION@),
 lalmetaio-dev (>= @MIN_LALMETAIO_VERSION@),
 libgsl-dev (>= 1.9),
 libgstreamer1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 libgstreamer-plugins-base1.0-0 (>= @MIN_GSTREAMER_VERSION@),
 python3-all-dev (>= @MIN_PYTHON_VERSION@),
 python3-gi, python-gi-dev
Description: Files and documentation needed for compiling gstlal-inspiral based plugins and programs.
 This package contains the files needed for building gstlal-inspiral based
 plugins and programs.
