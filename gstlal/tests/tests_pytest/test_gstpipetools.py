"""Unit tests for pipetools module"""


import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
from lal import LIGOTimeGPS


from gstlal import gstpipetools
from gstlal.pipeparts import pipetools, source, sink
from gstlal.utilities import testtools


class TestGsttools:
	"""Test class group for gsttools"""

	def test_is_element(self):
		"""Test is_element utility"""
		assert not gstpipetools.is_element(1)
		with testtools.GstLALTestManager(with_pipeline=True) as tm:
			elem = pipetools.make_element_with_src(tm.pipeline, None, 'fakesrc')
			assert gstpipetools.is_element(elem)

	def test_is_pad(self):
		"""Test is_pad utility"""
		assert not gstpipetools.is_element(1)
		with testtools.GstLALTestManager(with_pipeline=True) as tm:
			elem = pipetools.make_element_with_src(tm.pipeline, None, 'fakesrc')
			assert not gstpipetools.is_pad(elem)
			assert gstpipetools.is_pad(elem.pads[0])

	def test_to_caps(self):
		"""Test caps coercion"""
		c = gstpipetools.to_caps("audio/x-raw, format=F32LE")
		assert isinstance(c, Gst.Caps)

		c = gstpipetools.to_caps(c)
		assert isinstance(c, Gst.Caps)

	def test_make_element(self):
		"""Test make element"""
		e = gstpipetools.make_element('fakesrc')
		assert isinstance(e, Gst.Element)

	def test_make_pipeline(self):
		"""Test make pipeline"""
		p = gstpipetools.make_pipeline('SamplePipeline')
		assert isinstance(p, Gst.Pipeline)

	def test_run_pipeline(self):
		"""Test run pipeline"""
		pipeline = gstpipetools.make_pipeline('SamplePipelineRun')
		head = source.audio_test(pipeline, wave=source.AudioTestWaveform.Sine)
		head = sink.fake(pipeline, head)
		gstpipetools.run_pipeline(pipeline, segment=(0, 1))

	def test_seek(self):
		"""Test seek pipeline"""
		p = gstpipetools.make_pipeline('SamplePipeline')
		p.set_state(Gst.State.READY)
		gstpipetools.seek(p, segment=gstpipetools.LIVE_SEGMENT)

	def test_seek_args(self):
		"""Test seek args"""
		assert gstpipetools.seek_args(None) == (Gst.SeekType.NONE, -1)
		assert gstpipetools.seek_args(LIGOTimeGPS(123456)) == (Gst.SeekType.SET, LIGOTimeGPS(123456).ns())
		assert gstpipetools.seek_args(123456) == (Gst.SeekType.SET, int(float(123456) * Gst.SECOND))
