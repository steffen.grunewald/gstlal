"""Unittests for pipedot pipeparts"""
import re

import pytest

from gstlal import gstpipetools
from gstlal.pipeparts import pipetools, pipedot
from gstlal.pipeparts.pipetools import Gst, GObject
from gstlal.utilities import testtools

# 'fakesrc0_0x7fae93190120_src_0x7fae93194080'
MEMORY_ADDR_PATTERN_OSX = '[0-9]?_0x[0-9a-fA-F]{12}'
MEMORY_ADDR_PATTERN_LINUX = '[0-9]?_0x[0-9a-fA-F]{7}'
FAKE_SRC_PATTERN = 'fakesrc[0-9]{1}'

CLEAN_PIPELINE_DOT_OSX = (
	'digraph pipeline {\n  rankdir=LR;\n  fontname="sans";\n  fontsize="10";\n  labelloc=t;\n  nodesep=.1;\n  ranksep=.2;\n  label="<GstPipeline>\\nTestEncode\\n[0]";\n'
	'  node [style="filled,rounded", shape=box, fontsize="9", fontname="sans", margin="0.0,0.0"];\n  edge [labelfontsize="6", fontsize="9", fontname="monospace"];\n  \n'
	'  legend [\n    pos="0,0!",\n    margin="0.05,0.05",\n    style="filled",\n    label="Legend\\lElement-States: [~] void-pending, [0] null, [-] ready, [=] paused, [>]'
	' playing\\lPad-Activation: [-] none, [>] push, [<] pull\\lPad-Flags: [b]locked, [f]lushing, [b]locking, [E]OS; upper-case is set\\lPad-Task: [T] has started task, [t]'
	' has paused task\\l",\n  ];\n  subgraph cluster_fakesrc {\n    fontname="Bitstream Vera Sans";\n    fontsize="8";\n    style="filled,rounded";\n    color=black;\n    '
	'label="GstFakeSrc\\nfakesrc\\n[0]\\nfilltype=nothing";\n    subgraph cluster_fakesrc_src {\n      label="";\n      style="invis";\n      fakesrc_src [color=black, '
	'fillcolor="#ffaaaa", label="src\\n[-][bFb]", height="0.2", style="filled,solid"];\n    }\n\n    fillcolor="#ffaaaa";\n  }\n\n}\n')

CLEAN_PIPELINE_DOT_LINUX = (
	'digraph pipeline {\n  rankdir=LR;\n  fontname="sans";\n  fontsize="10";\n  labelloc=t;\n  nodesep=.1;\n  ranksep=.2;\n  label="<GstPipeline>\\nTestEncode\\n[0]";\n'
	'  node [style="filled,rounded", shape=box, fontsize="9", fontname="sans", margin="0.0,0.0"];\n  edge [labelfontsize="6", fontsize="9", fontname="monospace"];\n  \n'
	'  legend [\n    pos="0,0!",\n    margin="0.05,0.05",\n    style="filled",\n    label="Legend\\lElement-States: [~] void-pending, [0] null, [-] ready, [=] paused, [>]'
	' playing\\lPad-Activation: [-] none, [>] push, [<] pull\\lPad-Flags: [b]locked, [f]lushing, [b]locking, [E]OS; upper-case is set\\lPad-Task: [T] has started task, [t]'
	' has paused task\\l",\n  ];\n  subgraph cluster_fakesrc {\n    fontname="Bitstream Vera Sans";\n    fontsize="8";\n    style="filled,rounded";\n    color=black;\n    '
	'label="GstFakeSrc\\nfakesrc\\n[0]\\nparent=(GstPipeline) TestEncode\\nfilltype=nothing";\n    subgraph cluster_fakesrc_src {\n      label="";\n      style="invis";\n      fakesrc_src [color=black, '
	'fillcolor="#ffaaaa", label="src\\n[-][bFb]", height="0.2", style="filled,solid"];\n    }\n\n    fillcolor="#ffaaaa";\n  }\n\n}\n')

SANITIZE_PATTERNS = [
	(MEMORY_ADDR_PATTERN_OSX, ''),
	(MEMORY_ADDR_PATTERN_LINUX, ''),
	(FAKE_SRC_PATTERN, 'fakesrc')
]

if testtools.is_osx():
	CLEAN_PIPELINE_DOT = CLEAN_PIPELINE_DOT_OSX
else:
	CLEAN_PIPELINE_DOT = CLEAN_PIPELINE_DOT_LINUX


def assert_elem_props(elem, name):
	assert gstpipetools.is_element(elem)
	assert type(elem).__name__ == name


def sanitize_dot_str(s: str) -> str:
	"""Utility for making a dot str stable across sessions"""
	for pat, rep in SANITIZE_PATTERNS:
		s = re.sub(pat, rep, s)
	return s


class TestSanitize:
	"""Test group for sanitizing dot strings"""

	def test_memory_addr_pattern(self):
		"""Test memory address pattern"""
		sample_text = 'asdljkhads asd fakesrc0_0x7fae93190120_src_0x7fae93194080 asldkj'
		matches = re.findall(re.compile(MEMORY_ADDR_PATTERN_OSX), sample_text)
		assert matches == ['0_0x7fae93190120', '_0x7fae93194080']

		sample_text = 'cluster_fakesrc_0x2cc08c0'
		matches = re.findall(re.compile(MEMORY_ADDR_PATTERN_LINUX), sample_text)
		assert matches == ['_0x2cc08c0']

	def test_sanitize_dot_str(self):
		"""Test sanitizer"""
		sample_text = 'asdljkhads asd fakesrc0_0x7fae93190120_src_0x7fae93194080 asldkj'
		sanitized = sanitize_dot_str(sample_text)
		assert sanitized == 'asdljkhads asd fakesrc_src asldkj'

		sample_text = 'cluster_fakesrc_0x2cc08c0'
		sanitized = sanitize_dot_str(sample_text)
		assert sanitized == 'cluster_fakesrc'


class TestDot:
	"""Test dot pipeparts"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		GObject.MainLoop()
		return Gst.Pipeline('TestEncode')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	@testtools.broken('Unable to redirect dump dot dir env var sufficiently')
	def test_to_file(self, pipeline, src):
		"""Test write dot file"""

		with testtools.GstLALTestManager() as tm:
			tm.override_env_var('GST_DEBUG_DUMP_DOT_DIR', tm.tmp_path.as_posix())

			pipedot.to_file(pipeline, 'graph')

			output_path = tm.tmp_path / 'graph.dot'
			assert output_path.exists()

			tm.reset_env_var('GST_DEBUG_DUMP_DOT_DIR')

	def test_to_file_use_str(self, pipeline, src):
		"""Test write dot file using str intermediary"""

		with testtools.GstLALTestManager() as tm:
			tm.override_env_var('GST_DEBUG_DUMP_DOT_DIR', tm.tmp_path.as_posix())

			pipedot.to_file(pipeline, 'graph', use_str_method=True)

			output_path = tm.tmp_path / 'graph.dot'
			assert output_path.exists()

	def test_to_str(self, pipeline, src):
		"""Test convert pipeline to str"""

		with testtools.GstLALTestManager() as tm:
			res = pipedot.to_str(pipeline)
			assert isinstance(res, str)
			assert sanitize_dot_str(res)[:10] == CLEAN_PIPELINE_DOT[:10] # TODO reverse kneecap
